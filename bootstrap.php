<?php
/**
 * ExtModule
 *
 * Extended Module Manager
 *
 * @package		ExtModule
 * @version		0.1
 * @author		T.Ikeda <im.ikeda@gmail.com>
 */

\Autoloader::add_core_namespace('ExtModule');

\Autoloader::add_classes(array(
	'ExtModule\\Menu'			=> __DIR__.'/classes/menu.php',

	'ExtModule\\Menu_Base'		=> __DIR__.'/classes/menu/base.php',
	'ExtModule\\Menu_Badge'		=> __DIR__.'/classes/menu/badge.php',
	'ExtModule\\Menu_Icon'		=> __DIR__.'/classes/menu/icon.php',
	'ExtModule\\Menu_Item'		=> __DIR__.'/classes/menu/item.php',
	'ExtModule\\Menu_Treeview'	=> __DIR__.'/classes/menu/treeview.php',
	'ExtModule\\Menu_Sidemenu'	=> __DIR__.'/classes/menu/sidemenu.php',

	'ExtModule\\Installer'		=> __DIR__.'/classes/installer.php',
));
