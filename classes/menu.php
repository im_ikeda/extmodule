<?php
/**
 * ExtModule
 *
 * Extended Module Manager
 *
 * @package		ExtModule
 * @version		0.1
 * @author		T.Ikeda <im.ikeda@gmail.com>
 */

namespace ExtModule;

/**
 *
 * Class: ExtModule\Menu - Tiny Menu Generator
 *
 *	
 * @package	ExtModule
 * @author	Takashi Ikeda <t-ikeda@mdb.co.jp>
 * @date	2016/08/11
 * @version	0.1
 *
 */

class Menu {
	const	CACHE_TTL	= 3600;		// 1 hour

	public static function generate($menuconfig, $cachettl = NULL) {
		$result = '';

		if ($cachettl === NULL) {
			$cachettl = self::CACHE_TTL;
		}

		$req = \Request::main();
		$current = array(
			'module' => strtolower($req->module),
			'controller' => preg_replace('|^controller_|', '',
								strtolower($req->controller)),
			'action' => strtolower($req->action),
		);
		$cachekey = static::_cache_key(\Arr::merge($current, $menuconfig));

		try {
			$cached = \Cache::Get($cachekey);
		} catch (\CacheNotFoundException $e) {
			// No Cache
			$cached = '';
		}
		if (!empty($cached)) {
			return $cached;
		}

		foreach ($menuconfig as $link => $attrs) {
			if (!is_array($attrs)) {
				$attrs = array(
					'type'	=> 'item',
					'label'	=> $attrs,
					'href'	=> $link,
				);
			}
			$type = \Arr::get($attrs, 'type', 'item');

			$class = 'Menu_'. ucfirst($type);
			$listitem = $class::generate($attrs, $current);

			$isactive = ($current['module'] == strtolower($link));

			switch ($type) {
			case 'item':
				$result .= html_tag('li', array(
								'class' => $isactive ? 'active' : '',
							), $listitem);
				break;
			case 'treeview':
				$result .= html_tag('li', array(
								'class' => 'treeview'. ($isactive ? ' active' : ''),
							), $listitem);
				break;
			case 'sidemenu':
				$liattr = \Arr::get($attrs, 'attr', array(
									'class'	=> 'sidemenu',
								));
				$liattr['class'] .= ($isactive ? ' active' : '');
				$result .= html_tag('li', $liattr, $listitem);
				break;
			}
		}

		\Cache::set($cachekey, $result, $cachettl);
		return $result;
	}

	protected static function _cache_key($keys) {
		$flatted = \Arr::flatten($keys, ':', TRUE);
		$keyitems = array();
		foreach ($flatted as $key => $val) {
			$keyitems[] = "{$key}-{$val}";
		}
		return sha1(implode("_", $keyitems));
	}

	public static function menuRender($menuconfig = NULL,
									$cachettl = NULL) {
		if (empty($menuconfig)) {
			$menuconfig = \Config::load('menus');
		}
		return static::generate($menuconfig, $cachettl);
	}
}
