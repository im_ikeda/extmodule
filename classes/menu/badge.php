<?php
/**
 * ExtModule
 *
 * Extended Module Manager
 *
 * @package		ExtModule
 * @version		0.1
 * @author		T.Ikeda <im.ikeda@gmail.com>
 */

namespace ExtModule;

/**
 *
 * Class: ExtModule\Menu_Badge - Tiny Menu Generator
 *
 *	
 * @package	ExtModule
 * @author	Takashi Ikeda <im.ikeda@gmail.com>
 * @date	2016/08/11
 * @version	0.1
 *
 */
class Menu_Badge extends Menu_Base {
	public static function generate($config, $current) {
		if (!is_array($config)) {
			$config = array(
				'tag'		=> 'small',
				'class'		=> $config,
				'nospace'	=> false,
				'content'	=> ' ',
			);
		}
		$tag		= \Arr::get($config, 'tag', 'small');
		$class		= \Arr::get($config, 'class');
		$content	= \Arr::get($config, 'content', ' ');
		$before		= \Arr::get($config, 'nospace', false) ? '' : ' ';

		return $before . html_tag($tag, array(
										'class'	=> $class,
									), $content);
	}
}
