<?php
/**
 * ExtModule
 *
 * Extended Module Manager
 *
 * @package		ExtModule
 * @version		0.1
 * @author		T.Ikeda <im.ikeda@gmail.com>
 */

namespace ExtModule;

/**
 *
 * Class: ExtModule\Menu_Sidemenu - Tiny Menu Generator
 *
 *	
 * @package	ExtModule
 * @author	Takashi Ikeda <im.ikeda@gmail.com>
 * @date	2016/08/11
 * @version	0.1
 *
 */
class Menu_Sidemenu extends Menu_Base {
	public static function generate($config, $current) {
		$contents = '';

		$label	= \Arr::get($config, 'label');
		$wrapper= \Arr::get($config, 'wrapper', array());
		$icon	= \Arr::get($config, 'icon');
		$badge	= \Arr::get($config, 'badge');
		$items	= \Arr::get($config, 'submenus');
		$secure	= \Arr::get($config, 'secure', NULL);

		if (!empty($icon)) {
			$icon = Menu_Icon::generate($icon, $current);
		}
		if (!empty($badge)) {
			$badge = Menu_Badge::generate($badge);
		}

		$anchorattr = array();
		if ($current['module'] ===
				\Arr::get($config, 'controller', FALSE)) {
			$anchorattr['class'] = 'active';
		}
		$contents = \Html::anchor('#',
							 static::wrap($wrapper,
									 $icon .
									 html_tag('span', array(), $label) .
									 $badge),
							 $anchorattr, $secure);
		$subattrs = \Arr::get($items, 'attr', array());
		unset($items['attr']);

		$submenus = '';
		foreach ($items as $idx => $itemconfig) {
			$attrs = \Arr::get($itemconfig, 'attr', array());
			$submenus .= html_tag('li', $attrs,
								Menu_Item::generate($itemconfig, $current));
		}

		$class = \Arr::get($subattrs, 'class');
		if (empty($class)) {
			$subattrs['class'] = 'sidemenu';
		}

		$contents .= html_tag('ul', $subattrs, $submenus);

		return $contents;
	}
}
