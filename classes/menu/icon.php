<?php
/**
 * ExtModule
 *
 * Extended Module Manager
 *
 * @package		ExtModule
 * @version		0.1
 * @author		T.Ikeda <im.ikeda@gmail.com>
 */

namespace ExtModule;

/**
 *
 * Class: ExtModule\Menu_Icon - Tiny Menu Generator
 *
 *	
 * @package	ExtModule
 * @author	Takashi Ikeda <im.ikeda@gmail.com>
 * @date	2016/08/11
 * @version	0.1
 *
 */
class Menu_Icon extends Menu_Base {
	public static function generate($config, $current) {
		if (!is_array($config)) {
			$config = array(
				'tag'		=> 'i',
				'class'		=> $config,
				'nospace'	=> false,
			);
		}
		$tag	= \Arr::get($config, 'tag');
		$class	= \Arr::get($config, 'class');
		$after	= \Arr::get($config, 'nospace', false) ? '' : ' ';

		return html_tag($tag, array(
							'class'	=> $class,
						), ''). $after;
	}
}
