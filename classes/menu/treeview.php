<?php
/**
 * ExtModule
 *
 * Extended Module Manager
 *
 * @package		ExtModule
 * @version		0.1
 * @author		T.Ikeda <im.ikeda@gmail.com>
 */

namespace ExtModule;

/**
 *
 * Class: ExtModule\Menu_Treeview - Tiny Menu Generator
 *
 *	
 * @package	ExtModule
 * @author	Takashi Ikeda <im.ikeda@gmail.com>
 * @date	2016/08/11
 * @version	0.1
 *
 */
class Menu_Treeview extends Menu_Base {
	public static function generate($config, $current) {
		$contents = '';

		$label	= \Arr::get($config, 'label');
		$attrs	= \Arr::get($config, 'attr', array());
		$icon	= \Arr::get($config, 'icon');
		$badge	= \Arr::get($config, 'badge');
		$items	= \Arr::get($config, 'submenus');
		$secure	= \Arr::get($config, 'secure', NULL);

		if (!empty($icon)) {
			$icon = Menu_Icon::generate($icon, $current);
		}
		if (!empty($badge)) {
			$badge = Menu_Badge::generate($badge, $current);
		}

		$contents = \Html::anchor('#',
						 $icon . html_tag('span', array(), $label) . $badge,
						 $attrs, $secure);
		$submenus = '';
		foreach ($items as $idx => $itemconfig) {
			$submenus .= html_tag('li', array(),
								Menu_Item::generate($itemconfig, $current));
		}
		$contents .= html_tag('ul', array(
							'class'	=> 'treeview-menu',
						), $submenus);

		return $contents;
	}
}
