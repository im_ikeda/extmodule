<?php
/**
 * ExtModule
 *
 * Extended Module Manager
 *
 * @package		ExtModule
 * @version		0.1
 * @author		T.Ikeda <im.ikeda@gmail.com>
 */

namespace ExtModule;

/**
 *
 * Class: ExtModule\Menu_Base - Tiny Menu Generator
 *
 *	
 * @package	ExtModule
 * @author	Takashi Ikeda <im.ikeda@gmail.com>
 * @date	2016/08/11
 * @version	0.1
 * @abstract
 *
 */
abstract class Menu_Base {
	public static function generate($config, $current) {
	}

	public static function toLink($hrefs) {
		if (is_string($hrefs)) return $hrefs;

		if (!is_array($hrefs)) {
			return "";
		}

		$module = \Arr::get($hrefs, 'module');
		$controller = \Arr::get($hrefs, 'controller');
		$action = \Arr::get($hrefs, 'action', '');

		$basehref = $controller .'/'. $action;
		return empty($module) ?  $basehref : $module .'/'. $basehref;
	}

	public static function wrap($config, $contents) {
		if (empty($config)) {
			return $contents;
		}

		$tag	= \Arr::get($config, 'tag', 'span');
		$attrs	= \Arr::get($config, 'attr', array());

		return html_tag($tag, $attrs, $contents);
	}
}
