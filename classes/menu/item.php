<?php
/**
 * ExtModule
 *
 * Extended Module Manager
 *
 * @package		ExtModule
 * @version		0.1
 * @author		T.Ikeda <im.ikeda@gmail.com>
 */

namespace ExtModule;

/**
 *
 * Class: ExtModule\Menu_Item - Tiny Menu Generator
 *
 *	
 * @package	ExtModule
 * @author	Takashi Ikeda <im.ikeda@gmail.com>
 * @date	2016/08/11
 * @version	0.1
 *
 */
class Menu_Item extends Menu_Base {
	public static function generate($config, $current) {
		$label	= \Arr::get($config, 'label');
		$href 	= \Arr::get($config, 'href', array());

		$link	= static::toLink((empty($href) ? '#' : $href));

		$attrs	= \Arr::get($href, 'attr', array());
		$wrapper= \Arr::get($config, 'wrapper', array());
		$secure	= \Arr::get($config, 'secure', NULL);
		$icon	= \Arr::get($config, 'icon');
		$badge	= \Arr::get($config, 'badge');

		if (!empty($icon)) {
			$icon = Menu_Icon::generate($icon, $current);
		}
		if (!empty($badge)) {
			$badge = Menu_Badge::generate($badge, $current);
		}

		$controller = \Arr::get($href, 'controller');
		$action = \Arr::get($href, 'action');

		if ($current['controller'] == strtolower($controller) &&
			(!empty($action) &&
				$current['action'] == strtolower($action))) {
			$attrs['class'] .= ' active';
		}
		return \Html::anchor($link,
						static::wrap($wrapper, $icon . $label . $badge),
						$attrs, $secure);
	}
}
