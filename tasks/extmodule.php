<?php
namespace Fuel\Tasks;

/**
 *
 * Class: Extmodule - Extended Module
 *
 *
 * @author	Takashi Ikeda <t-ikeda@mdb.co.jp>
 * @date	2016/04/11
 * @version	0.1
 *
 */
class Extmodule {
	protected static	$identify = 'Extmodule';

	public static function run($modules = null) {
		static::install($modules);
	}

	public static function install($modules = null) {
		if (empty($modules)) {
			$modules = \File::read_dir(APPPATH .'modules/', 1, null);
		}

		foreach (array_keys($modules) as $modname) {
			static::_install_one(rtrim($modname, "/ \t\n\r\0\x0B"));
		}
	}

	protected static function _install_one($ident) {
		if (empty($ident) || !\Module::exists($ident)) {
			return false;
		}

		$loaded = TRUE;
		if (!\Module::loaded($ident)) {
			$loaded = FALSE;
			\Module::load($ident);
		}

		$meconf	 = \Config::load("{$ident}::module", $ident, true);
		\Config::load('modules', 'modules', true);
		\Config::set("modules.{$ident}", $meconf);
		$result = \Config::save('modules', 'modules');

		$memenu = \Config::get("{$ident}.menu", array());
		if (!empty($memenu)) {
			\Config::load('menus', 'menus');

			foreach ($memenu as $menuident => $menudata) {
				\Config::set("menus.{$menuident}", $menudata);
			}
			\Config::save('menus', 'menus');
		}

		$mewidgets = \Config::get("{$ident}.widgets", array());
		if (!empty($mewidgets)) {
			\Config::load('widgets', 'widgets');

			foreach ($mewidgets as $widident => $widdata) {
				\Config::set("widgets.{$widident}", $widdata);
			}
			\Config::save('widgets', 'widgets');
		}

		if (!$loaded) {
			\Module::unload($ident);
		}
	}

	public static function tests() {
		$menuconf = \Config::load("menus", "menus");

		$result = \ExtModule\Menu::generate($menuconf);

		echo "RESULT\n", $result, "\n";
	}
}
